﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Calculator
{
    public partial class MainPage : ContentPage
    {
        int currentState = 1;
        string myoperator;
        double firstNumber, secondNumber;

        //public object OperatorHelper { get; private set; }

        public MainPage()
        {
            InitializeComponent();
            OnClear(this, null);
        }

        void OnSelectNumber(object sender, EventArgs e)
        {
            Button button = (Button)sender;


            string pressed = button.Text;
            //validation 


            if (this.resultText.Text == "0" || currentState < 0)//at first current state is 1
            {
                this.resultText.Text = "";

                if (currentState < 0)
                    currentState *= -1;
            }

            this.resultText.Text += pressed;
            double number;
            if (double.TryParse(this.resultText.Text, out number))
            {
                this.resultText.Text = number.ToString("N0");
                if (currentState == 1)
                {
                    firstNumber = number;
                }
                else
                {
                    secondNumber = number;
                }
            }




        }

        void OnSelectOperator(object sender, EventArgs e)//when select operator is called 
        {
            currentState = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            myoperator = pressed;
        }

        void OnClear(object sender, EventArgs e)//  when we press the AC button
        {
            firstNumber = 0;
            secondNumber = 0;
            currentState = 1;
            this.resultText.Text = "0";
        }

        
        void OnCalculate(object sender, EventArgs e) 
        {
            if (currentState == 2)
            {
                var result = OperatorHelper.Calculate(firstNumber, secondNumber, myoperator);

                this.resultText.Text = result.ToString();
                firstNumber = (double)result;
                currentState = -1;
            }
        }
        void OnSquareRoot(object sender, EventArgs e) // square root of that number
        {
            if ((currentState == -1) || (currentState == 1))
            {
                //var result = OperatorHelper.MySquareRoot(firstNumber, myoperator);
                var result = Math.Sqrt(firstNumber);

                this.resultText.Text = result.ToString();
                firstNumber = result;
                currentState = -1;
            }
        }


        private void squareclicked(object sender, EventArgs e)// the square root of that number
        {

            if ((currentState == -1) || (currentState == 1))
            {
                //var result = OperatorHelper.MySquare(firstNumber, myoperator);
                var result = firstNumber * firstNumber;
                this.resultText.Text = result.ToString();
                firstNumber = result;
                currentState = -1;
            }
        }

    }
}

